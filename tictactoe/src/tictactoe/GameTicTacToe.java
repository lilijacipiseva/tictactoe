package tictactoe;
/*
Hi! We are the team Lilija and Aleksandra. We chose TicTacToe for the final test. 
Here is the result :
The first player is Human, but the second one - Computer.

 */

import java.util.Random; // The first two lines are class imports.
import java.util.Scanner;

public class GameTicTacToe { // First in the class body are the descriptions
								// of the fields, then the methods.

	final char SIGN_X = 'x';	// We use three character constants as fields: SIGN_X, SIGN_O, and SIGN_EMPTY.
	final char SIGN_O = 'o'; 	// Their values cannot be changed, as indicated
	final char SIGN_EMPTY = '_';// by the "final" modifier.
	char[][] board;				// The two-dimensional array "board" will be our playing field.
	Random random; 				// We also need a random object to generate Computer moves
	Scanner scanner; 			// and a scanner to enter data from the Human.

	public static void main(String[] args) { 	// The main() method is used to create an object,
												// to call the gameTicTacToe() method with game logic,
		instructionBoard(); 					// and to call the method instructionBoard()
												// with the "board" coordinates instruction.
		new GameTicTacToe().gameTicTacToe();

	}

	GameTicTacToe() { 							
		random = new Random(); 					// We use the constructor to initialise the fields. The name is the same as the class name.								
		scanner = new Scanner(System.in);
		board = new char[3][3];
	}

	public static void instructionBoard() {		// Here is the method instructionBoard() with the "board" coordinates instruction.
		System.out.println("Instruction board with coordinates");
		System.out.println("| x=1, y=1 | x=2, y=1 | x=3, y=1 |");
		System.out.println("| x=1, y=2 | x=2, y=2 | x=3, y=2 |");
		System.out.println("| x=1, y=3 | x=2, y=3 | x=3, y=3 |");
		System.out.println();
	}

	void gameTicTacToe() {  // The game logic is located in the gameTicTacToe() method and
							// is based on an infinite while loop.
							// The sequence of actions:
		initBoard(); 		// The "board" initializing

		while (true) {
			turnHuman(); 						// Human's turn
			if (checkWin(SIGN_X)) { 			// Check: if Human wins or it's a draw
				System.out.println("YOU WIN!"); // Inform and exit the loop
				break;
			}
			if (isBoardFull()) {
				System.out.println("Sorry, DRAW!");
				break;
			}
			turnAI(); 							// Computer's turn
			printBoard(); 						// Print the board
			if (checkWin(SIGN_O)) { 			// Check: if Computer wins or it's a draw
				System.out.println("AI WIN!");	// Inform and exit the loop
				break;
			}
			if (isBoardFull()) {
				System.out.println("Sorry, DRAW!");
				break;
			}
		}
		System.out.println("GameTicTacToe OVER.");  // If there is a winning or draw situation
		printBoard(); 								// (all the cells of the board are filled),
	} 												// we exit the loop with the "break", ending the game.

													// Methods that are called in the gameTicTacToe():
	void initBoard() { 								// The first one, initBoard(), provides the initial initialization of the game board,
													// filling its cells with "empty" "_" characters.
		for (int row = 0; row < 3; row++) 			// The outer loop, with the "int row" counter, selects row
			for (int col = 0; col < 3; col++) 		// and the inner loop, with the "int col" counter, iterates through the
													// cells in each row.
				board[row][col] = SIGN_EMPTY;
	}

	void printBoard() { 							// Method printBoard() displays the current state of the game board.
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++)
				System.out.print(board[row][col] + " ");
			System.out.println();
		}
	}

	void turnHuman() { 								// In the turnHuman() method, which allows the Human to make a move,
		int x, y; 									// we use the nextInt() method of the scanner object to read two integers
													// (coordinates) from the console.
		do { 										// the do-while loop: the coordinate request is repeated if the user specifies
													// the coordinates of an invalid cell
													// (the board cell is occupied or does not exist).
			System.out.println("Enter coordinates:");
			System.out.println("Enter x (1,2,3):");
			x = scanner.nextInt() - 1;
			System.out.println("Enter y (1,2,3):");
			y = scanner.nextInt() - 1;
		} while (!isCellValid(x, y));
		board[y][x] = SIGN_X; 						// If everything is fine with the cell,
	} 												// the SIGN_X symbol — "cross" - is entered there.

	boolean isCellValid(int x, int y) { 			// The validity of the cell is determined by the method isCellValid()
													// It returns a Boolean value: true-if the cell is free and exists,
		if (x < 0 || y < 0 || x >= 3 || y >= 3) { 	// false-if the cell is occupied or incorrect coordinates are
													// specified.
			System.out.println("Input is incorect. Please, enter value 1, 2, 3! ");
			return false;
		}
		return board[y][x] == SIGN_EMPTY;
	}

	void turnAI() { 								// The turnAI() method is similar to the turnHuman() method using a do-while loop.													
		int x, y;									// This method allows the Computer to make a move.
		do {
			x = random.nextInt(3); 					// The cell coordinates are not read from the console, but are generated randomly													
			y = random.nextInt(3); 					// using the nextInt(3) method of the random object.
													// The number 3 passed as a parameter is a limiter. Random integers from 0 to 2
													// are generated (within the indexes of the game board array).
		} while (!isCellValid(x, y)); 				// The isCellValid() method allows to select only free cells for entering the
													// SIGN_O - "nought" sign in them.
		board[y][x] = SIGN_O;
	}

	// The CheckWin() method checks the game board for a "winning three" - three
	// identical characters in a row, vertically or horizontally (in a loop), as
	// well as on two diagonals.
	// The checked sign is specified as the char "underscore" parameter, the method
	// is universal - we can check the winning by both "crosses" and "noughts".
	// First, the "cross" symbol is passed to the method, and then the same method
	// is called, but the "nought" symbol is passed to it.
	// In case of winning, the Boolean value true is returned, otherwise false.
	boolean checkWin(char underscore) {
		for (int i = 0; i < 3; i++)
			if ((board[i][0] == underscore && board[i][1] == underscore && board[i][2] == underscore)
					|| (board[0][i] == underscore && board[1][i] == underscore && board[2][i] == underscore))
				return true;
		if ((board[0][0] == underscore && board[1][1] == underscore && board[2][2] == underscore)
				|| (board[2][0] == underscore && board[1][1] == underscore && board[0][2] == underscore))
			return true;
		return false;
	}

	boolean isBoardFull() { 						// The is isBoardFull() method runs through all the cells of the game board in 
													// a double loop and if at least one cell is still free, false is returned,
		for (int row = 0; row < 3; row++) 			// if they are all occupied, returns true.
			for (int col = 0; col < 3; col++)
				if (board[row][col] == SIGN_EMPTY)
					return false;
		return true;
	}

}
